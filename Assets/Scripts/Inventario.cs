﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventario : MonoBehaviour {
    public GameObject inventario;
    public Transform painel;
    public static bool pegou = false;
    private static int qtdSlots;
    private static Transform[] slots;
    public static GameObject zero;
    public static Texture zeroo;
    // Use this for initialization
    void Start () {
        qtdSlots = painel.childCount;
        slots = new Transform[qtdSlots];
        DetectaSlots();
	}
	
	// Update is called once per frame
	public void Update () {

		if(Input.GetKeyDown(KeyCode.I)){
            if(inventario.GetComponent<CanvasGroup>().alpha == 1)
            {
                inventario.GetComponent<CanvasGroup>().alpha = 0;
                Time.timeScale = 1;
            }
            else
            {
                inventario.GetComponent<CanvasGroup>().alpha = 1;
                Time.timeScale = 0;
            }
        }
	}
    private void DetectaSlots()
    {
        for (int i=0; i < qtdSlots; i++){
            slots[i] = painel.GetChild(i);
            print(slots[i].name);
        }
    }
    public void AddItem(GameObject item)
    {
        for(int i = 0; i < qtdSlots; i++)
        {
            if(!slots[i].GetComponent<Slot>().ocupado){
                slots[i].GetComponent<Slot>().item = item;
                slots[i].GetComponent<Slot>().icone = item.GetComponent<Item>().icone;
                break;

            }
        }
    }
    public void OnTriggerEnter2D(Collider2D outro)
    {
        print("colisao");
        if(outro.tag == "Item")
        {
            pegou = true;
            print("colisao com item");
            GameObject itemcoletado = outro.gameObject;
            AddItem(itemcoletado);
            itemcoletado.transform.SetParent(this.transform);
            itemcoletado.SetActive(false);
            print("poha");
        }
    }
}

